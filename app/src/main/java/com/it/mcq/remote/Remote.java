package com.it.mcq.remote;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import com.it.mcq.customViews.ErrorDialogView;
import com.it.mcq.db.ServerDaoImpl;


public class Remote {

    private String serverUrl="http://192.168.1.70:8080";
    private ServerDaoImpl serverdao;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getServerUrl(Context context)
    {
        try {
            serverdao = new ServerDaoImpl(context);
            return "http://" + serverdao.getAll().get(0).serverAddress;
        }
        catch(IndexOutOfBoundsException e)
        {

        }
        catch(NullPointerException e)
        {

        }
        finally{
            return getServerUrl();
        }
    }

}
