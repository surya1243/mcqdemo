package com.it.mcq.db;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


import com.it.mcq.model.Server;

import java.util.List;

@Dao
public interface ServerDao {

    @Query("SELECT * FROM server")
    List<Server> getAll();

    @Query("SELECT * FROM server WHERE uid IN (:userIds)")
    List<Server> loadAllByIds(int[] userIds);


    @Insert
    void insertAll(Server... servers);

    @Delete
    void delete(Server user);

    @Query("DELETE FROM server")
    void removeAll();

}
