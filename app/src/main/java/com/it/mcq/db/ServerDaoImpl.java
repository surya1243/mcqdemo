package com.it.mcq.db;

import android.content.Context;


import com.it.mcq.model.Server;

import java.util.List;

public class ServerDaoImpl implements ServerDao {

    AppDatabase db;
    public ServerDaoImpl(Context context)
    {
        db=AppDatabase.getInstance(context);
    }
    @Override
    public List<Server> getAll() {
        return db.serverDao().getAll();
    }

    @Override
    public List<Server> loadAllByIds(int[] userIds) {
        return db.serverDao().loadAllByIds(userIds);
    }

    @Override
    public void insertAll(Server... servers) {
        db.serverDao().insertAll(servers);
    }

    @Override
    public void delete(Server server) {
        db.serverDao().delete(server);
    }

    @Override
    public void removeAll() {
        db.serverDao().removeAll();
    }

    public void removeAllServersAndAddNewServer(Server server)
    {
        removeAll();
        insertAll(server);
    }
}
