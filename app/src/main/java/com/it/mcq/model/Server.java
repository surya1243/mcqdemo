package com.it.mcq.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Server {
    @PrimaryKey
    public int uid;

    @ColumnInfo(name = "serverAddress")
    public String serverAddress;
}
