package com.it.mcq;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.mcq.db.ServerDaoImpl;
import com.it.mcq.model.Server;
import com.it.mcq.remote.Remote;

public class MainActivity extends AppCompatActivity {

    final Context context = this;
    private ImageView imgButton;
    private TextView result;
    private Button btnProceed;
    private ServerDaoImpl serverDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        serverDao = new ServerDaoImpl(getApplicationContext());
        // components from main.xml
        imgButton = (ImageView) findViewById(R.id.iconMain);
        result = (TextView) findViewById(R.id.showIpAddress);
        btnProceed = (Button) findViewById(R.id.btnProceed);

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, WebShowActivity.class);
                startActivity(intent);
            }
        });
        result.setText(new Remote().getServerUrl(getApplicationContext()));

        // add imgButton listener
        imgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.prompt_adapter_layout, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.serverIpAddress);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // get user input and set it to result
                                        // edit text
                                        Server server = new Server();
                                        server.serverAddress=userInput.getText().toString();
                                        serverDao.removeAllServersAndAddNewServer(server);

                                        result.setText(userInput.getText());
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });
    }

}
