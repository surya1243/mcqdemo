package com.it.mcq;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.it.mcq.remote.Remote;

public class WebShowActivity extends AppCompatActivity {

    WebView wikiView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_show);

        wikiView =findViewById(R.id.webView);
        String serverUrl =new Remote().getServerUrl(getApplicationContext());
        wikiView.setWebViewClient(new MyBrowser());
        wikiView.getSettings().setLoadsImagesAutomatically(true);
        wikiView.getSettings().setJavaScriptEnabled(true);
        wikiView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wikiView.loadUrl(serverUrl);

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
